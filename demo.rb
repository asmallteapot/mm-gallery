require 'rubygems'
require 'sinatra'
require 'net/http'
require 'uri'
require 'json'
require 'haml'

configure do
end


helpers do
	def object_from_uri(uri_string)
		uri = URI.parse(uri_string)
		raw_data = Net::HTTP.get(uri)
		JSON.parse(raw_data)
	end
	
	def mm_gallery_uri(mm_username)
		"http://gallery.me.com/#{mm_username}?webdav-method=truthget&feedfmt=json"
	end
	
	def mm_album_uri(mm_username, album_num)
		"http://gallery.me.com/#{mm_username}/#{album_num}?webdav-method=truthget&feedfmt=json"
	end
end


before do
	@mm_username = 'flpatriot'
end


get '/' do
	# list albums
	raw_data = object_from_uri(mm_gallery_uri(@mm_username))
	
	@albums = raw_data['records']
	@gallery = raw_data['data']
	@title = @gallery['title']
	
	haml :gallery
end


get '/albums/:album_num' do |album_num|
	raw_data = object_from_uri(mm_album_uri(@mm_username, album_num))
	
	@album = raw_data['records'][0]
	@album[:photos] = raw_data['records'][1..-1]
	@title = @album['title']
	
	haml :album
end